include config.mk

C_SRC:=$(wildcard $(SRCDIR)/*.c)
ASM_SRC:=$(wildcard $(SRCDIR)/$(ARCH)-$(OS)-$(USERSPACE)/*.asm)

SRC+=$(C_SRC) $(ASM_SRC)
OBJ+=$(patsubst $(SRCDIR)/%.c,$(OBJDIR)/%.c.o,$(C_SRC)) \
	$(patsubst $(SRCDIR)/$(ARCH)-$(OS)-$(USERSPACE)/%.asm,$(OBJDIR)/%.asm.o,$(ASM_SRC))
BIN=$(BUILD)/$(BINNAME).$(VERSION)
LIB+=
ROOT?=$(HOME)/.local

.PHONY: all install clean mrproper

all: $(OBJDIR) $(BUILD) $(BIN)

run: $(BIN)
	exec $(BIN)

install: all
	$(RUN) mkdir -p root/bin
	$(RUN) cp -r $(BIN) root/bin
	$(RUN) cp -r root/bin/$(BINNAME).$(VERSION) $(ROOT)/bin/$(BINNAME)

$(OBJDIR):
	$(RUN) mkdir -p $(OBJDIR)

$(BUILD):
	$(RUN) mkdir -p $(BUILD)

$(OBJDIR)/%.c.o: $(SRCDIR)/%.c
	$(RUN) $(CC) $(CFLAGS) $(INCLUDE) -c -o $@ $^

$(OBJDIR)/%.asm.o: $(SRCDIR)/$(ARCH)-$(OS)-$(USERSPACE)/%.asm
	$(RUN) $(AS) $(ASFLAGS) $(INCLUDE) -o $@ $^

$(BIN): $(OBJ) $(LIB)
	$(RUN) $(LD) $(LDFLAGS) -o $@ $^

clean:
	$(RUN) rm -rf target root

mrproper: clean
	$(RUN) rm -rf $(BINNAME)
