#define _GNU_SOURCE
#define _POSIX_C_SOURCE 1
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <sys/ptrace.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <pthread.h>
#include <ehrt/debug.h>

struct Debugger {
    pthread_mutex_t handle;
    int quit;
    EhDebugData data;
    FILE *pipe;
    char *arg0;
};

_Noreturn
void child(int argc, char **argv) {
    ptrace(PTRACE_TRACEME, 0, NULL, NULL);
    execvp(argv[1], argv + 1);
    perror(argv[1]);
    exit(-1);
}

void *reader_start(void *ptr) {
    struct Debugger *debugger = ptr;
    while (1) {
        void *ptr;
        int line;
        int column;
        char *file = NULL;
        char *ident = NULL;
        int n, err = n = fscanf(debugger->pipe, "0x%p %ms %m[^:]:%d:%d\n", &ptr, &ident, &file, &line, &column);
        if (err < 0) {
            if (errno) {
                perror(debugger->arg0);
                exit(err);
            } else {
                break;
            }
        }

        pthread_mutex_lock(&debugger->handle);
        if (debugger->quit) {
            pthread_mutex_unlock(&debugger->handle);
            if (n >= 0) {
                free(file);
                free(ident);
            }
            break;
        }

        if (n > 0) {
            EhDebugFrame frame;
            new_debug_frame(&frame, ptr, line, column, file, ident);
            ehdebug_push(&debugger->data, &frame);
        }
        if (n >= 0) {
            free(file);
            free(ident);
        }

        pthread_mutex_unlock(&debugger->handle);

        struct timespec tm;
        tm.tv_sec = 0;
        tm.tv_nsec = 100000;
        nanosleep(&tm, NULL);
    }
    pthread_exit(debugger);
}

char *make_pipe(char *arg0, pid_t pid) {
    int err;
    size_t size = sizeof("/tmp/ehdb.xxxxx");
    char *filename = malloc(size);
    snprintf(filename, size, "/tmp/ehdb.%u", pid);
    mode_t mask = umask(0);
    if ((err = mknod(filename, S_IFIFO | S_IRUSR | S_IWUSR, 0))) {
        perror(arg0);
        exit(err);
    }
    umask(mask);
    return filename;
}

void cont(pid_t pid) {
    ptrace(PTRACE_CONT, pid, NULL, NULL);

    int wstatus;
    waitpid(pid, &wstatus, 0);
}

int run(int argc, char **argv, pid_t child_pid) {
    char *filename = make_pipe(argv[0], child_pid);

    int wstatus;
    waitpid(child_pid, &wstatus, 0);
    ptrace(PTRACE_CONT, child_pid, NULL, NULL);
    int fd = open(filename, O_RDONLY);
    if (fd < 0) {
        perror(argv[0]);
        exit(fd);
    }
    waitpid(child_pid, &wstatus, 0);

    struct Debugger *debugger = malloc(sizeof(struct Debugger));

    debugger->arg0 = argv[0];
    debugger->quit = 0;
    debugger->pipe = fdopen(fd, "r");
    new_debug_data(&debugger->data, 0, 128, NULL);

    pthread_mutexattr_t mutex_attr;
    pthread_mutexattr_init(&mutex_attr);
    pthread_mutex_t handle = PTHREAD_MUTEX_INITIALIZER;
    debugger->handle = handle;
    pthread_mutex_init(&debugger->handle, &mutex_attr);
    pthread_mutexattr_destroy(&mutex_attr);

    pthread_t reader;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_create(&reader, &attr, reader_start, debugger);
    pthread_attr_destroy(&attr);

    char *input;
    size_t n;
    while (1) {
        fprintf(stdout, "ehdb> ");
        fflush(stdout);
        input = NULL;
        n = 0;
        ssize_t err = getline(&input, &n, stdin);
        if (err < 0) {
            perror(argv[0]);
            exit(err);
        }

        *strchr(input, '\n') = 0;
        if (!strcmp(input, "cont")) {
            cont(child_pid);
        } else if (!strcmp(input, "exit")) {
            kill(child_pid, SIGINT);
            free(input);
            break;
        }

        free(input);
    }

    pthread_mutex_lock(&debugger->handle);
    debugger->quit = 1;
    pthread_mutex_unlock(&debugger->handle);

    pthread_join(reader, (void **) &debugger);
    del_debug_data(&debugger->data);
    pthread_mutex_destroy(&debugger->handle);
    fclose(debugger->pipe);
    free(debugger);

    remove(filename);
    free(filename);

    return 0;
}

int main(int argc, char **argv) {
    if (argc < 2) {
        fprintf(stderr, "%s: expecting at least 1 parameter\n", argv[0]);
        return 1;
    }

    pid_t fk = fork();
    if (fk < 0) {
        perror(argv[0]);
        exit(fk);
    } else if (fk) {
        return run(argc, argv, fk);
    } else {
        child(argc, argv);
    }
}
