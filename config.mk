BINNAME?=ehdb
ARCH?=x86_64
OS?=linux
USERSPACE?=gnu
VERSION:=0.1.0
VERSION_SHORT:=0.1
VERSION_SHORTER:=0

SRCDIR:=src
INCLUDE:=-Iinclude/ -I$(SRCDIR)/

CC:=gcc
CXX:=g++
AS:=nasm
LD:=gcc

ifeq ($(RELEASE),1)
TARGET:=release
NDEBUG:=1
else
TARGET:=debug
endif

ifeq ($(OS),linux)
EXE64:=elf64
EXE32:=elf32
else ifeq ($(OS),windows)
EXE64:=win64
EXE32:=win32
else ifeq ($(OS),osx)
EXE64:=macho64
EXE32:=macho32
else
$(error unsuppored os \"$(ARCH)\")
endif # $(OS)

ifeq ($(ARCH),x86_64)
EXEFORMAT:=$(EXE64)
BITS:=64
LIBDIR:=lib64
else ifeq ($(ARCH),x86-64)
ARCH:=x86_64
EXEFORMAT:=$(EXE64)
BITS:=64
LIBDIR:=lib64
else ifeq ($(ARCH),amd64)
ARCH:=x86_64
EXEFORMAT:=$(EXE64)
BITS:=64
LIBDIR:=lib64
else ifeq ($(ARCH),x86)
EXEFORMAT:=$(EXE32)
BITS:=32
LIBDIR:=lib
else
$(error unsuppored architecture \"$(ARCH)\")
endif # $(ARCH)

ifdef NDEBUG
CFLAGS+=-DNDEBUG -O2
else
CFLAGS+=-DDEBUG -g -O0
ASFLAGS+=-g -F stabs
endif

CFLAGS+=-Wall -std=c99 -I$(HOME)/.local/include -pthread
CXXFLAGS+=$(CFLAGS)
ASFLAGS+=-f $(EXEFORMAT) 
LDFLAGS+=-L$(HOME)/.local/lib64 -lehrt -lm -lpthread -pthread

BUILD:=target/$(TARGET)/$(ARCH)-$(OS)-$(USERSPACE)
OBJDIR:=$(BUILD)/obj
